// import { Link, Outlet } from 'react-router-dom'
import { Tabs, TabList, TabPanels, Tab, TabPanel } from '@chakra-ui/react'
import { observer } from 'mobx-react'
import { useTranslation } from 'react-i18next'

import { Personal } from './pages/Personal/Personal'
import { ProjectList } from './pages/ProjectList/ProjectList'
import { Grades } from './pages/Grades/Grades'

import './App.scss'
import { ButtonLang } from './components/ButtonLang/ButtonLang'

/* <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
      </header> */

// <div className=''>
//   <nav>
//     <ul>
//       <li>
//         <Link to='/'>Home</Link>
//       </li>
//       <li>
//         <Link to='/contacts'>Contacts</Link>
//       </li>
//     </ul>
//   </nav>

//   <Outlet />
// </div>

interface ITabData {
  label: string
  component: () => JSX.Element
}

export const App = observer(() => {
  const { t } = useTranslation()

  const TabsData: ITabData[] = [
    {
      label: t('tabs.info'),
      component: Personal
    },
    {
      label: t('tabs.projects'),
      component: ProjectList
    },
    {
      label: t('tabs.grades'),
      component: Grades
    }
  ]

  return (
    <div className='App'>
      <div className='App-content'>
        <Tabs
          align='center'
          colorScheme='facebook'
          isFitted
          isLazy
          lazyBehavior='unmount'
        >
          <TabList>
            {TabsData.map((data, index) => (
              <Tab key={index}>{data.label}</Tab>
            ))}
          </TabList>

          <TabPanels mt={4}>
            {TabsData.map((data, index) => (
              <TabPanel key={index}>{data.component()}</TabPanel>
            ))}
          </TabPanels>
        </Tabs>
      </div>

      <div className='App-lang'>
        <ButtonLang />
      </div>
    </div>
  )
})
