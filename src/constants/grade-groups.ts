import { IGradeGroups } from "src/interfaces/skills";
import { Skill } from "./skills";

export const GradeGroups: IGradeGroups = {
  Common: {
    textCode: "grade-groups.common",
    className: "grade-group-common",
    data: [Skill.Language, Skill.OS],
  },
  Develop: {
    textCode: "grade-groups.development",
    className: "grade-group-development",
    data: [
      Skill.Front,
      Skill.Back,
      Skill.Styling,
      Skill.Database,
      Skill.VueLib,
      Skill.ReactLib,
      Skill.StyleLib,
      Skill.Other,
    ],
  },
  Support: {
    textCode: "grade-groups.support",
    className: "grade-group-support",
    data: [Skill.Tool, Skill.Build, Skill.Test, Skill.IDE],
  },
  Other: {
    textCode: "grade-groups.other",
    className: "grade-group-other",
    data: [Skill.Design, Skill.Office, Skill.Styleguide, Skill.Management],
  },
};
