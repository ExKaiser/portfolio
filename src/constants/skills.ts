export enum SkillColor {
  Green,
  Blue,
  Yellow,
  Pink,
}

export const OS = {
  Windows: { code: "os.windows", color: SkillColor.Yellow, value: 4 },
  MacOS: { code: "os.macos", color: SkillColor.Yellow, value: 3 },
  Linux: { code: "os.linux", color: SkillColor.Yellow, value: 1 },
};

export const Language = {
  JavaScript: { code: "language.javascript", color: SkillColor.Yellow, value: 5 },
  TypeScript: { code: "language.typescript", color: SkillColor.Yellow, value: 4 },
  HTML: { code: "language.html", color: SkillColor.Yellow, value: 5 },
  CSS: { code: "language.css", color: SkillColor.Yellow, value: 5 },
};

export const Build = {
  Webpack: { code: "build.webpack", color: SkillColor.Blue, value: 4 },
  Vite: { code: "build.vite", color: SkillColor.Blue, value: 4 },
  Gulp: { code: "build.gulp", color: SkillColor.Blue, value: 3 },
  Grunt: { code: "build.grunt", color: SkillColor.Blue, value: 1 },
};

export const Tool = {
  Git: { code: "tool.git", color: SkillColor.Blue, value: 4 },
  Gitlab: { code: "tool.gitlab", color: SkillColor.Blue, value: 4 },
  Github: { code: "tool.github", color: SkillColor.Blue, value: 3.5 },
  Docker: { code: "tool.docker", color: SkillColor.Blue, value: 2 },
  Kubernetes: { code: "tool.kubernetes", color: SkillColor.Blue, value: 0 },
};

export const Front = {
  Vue2: { code: "front.vue2", color: SkillColor.Green, value: 4.5 },
  Vue3: { code: "front.vue3", color: SkillColor.Green, value: 5 },
  React: { code: "front.react", color: SkillColor.Green, value: 3.5 },
  Angular: { code: "front.angular", color: SkillColor.Green, value: 1 },
  Jquery: { code: "front.jquery", color: SkillColor.Green, value: 3 },
  Lodash: { code: "front.lodash", color: SkillColor.Green, value: 3 },
  CoffeeScript: { code: "front.coffeescript", color: SkillColor.Green, value: 5 },
  Backbone: { code: "front.backbone", color: SkillColor.Green, value: 3 },
  Marionette: { code: "front.marionette", color: SkillColor.Green, value: 3.5 },
  Axios: { code: "front.axios", color: SkillColor.Green, value: 3.5 },
  Electron: { code: "front.electron", color: SkillColor.Green, value: 3.5 },
};

export const Back = {
  NestJs: { code: "back.nestjs", color: SkillColor.Green, value: 3.5 },
  Sequelize: { code: "back.sequelize", color: SkillColor.Green, value: 3 },
  PassportJs: { code: "back.passportjs", color: SkillColor.Green, value: 3 },
};

export const Styling = {
  Sass: { code: "styling.sass", color: SkillColor.Green, value: 5 },
  Scss: { code: "styling.scss", color: SkillColor.Green, value: 5 },
  Less: { code: "styling.less", color: SkillColor.Green, value: 4.5 },
  Stylus: { code: "styling.stylus", color: SkillColor.Green, value: 3 },
};

export const Database = {
  Postgres: { code: "database.postgres", color: SkillColor.Green, value: 3 },
};

export const VueLib = {
  Vuex: { code: "vuelib.vuex", color: SkillColor.Green, value: 5 },
  VueRouter: { code: "vuelib.vuerouter", color: SkillColor.Green, value: 5 },
  Vuetify: { code: "vuelib.vuetify", color: SkillColor.Green, value: 5 },
  Pinia: { code: "vuelib.pinia", color: SkillColor.Green, value: 5 },
  Quasar: { code: "vuelib.quasar", color: SkillColor.Green, value: 4 },
};

export const ReactLib = {
  ReactRouter: { code: "reactlib.reactrouter", color: SkillColor.Green, value: 4 },
  Mobx: { code: "reactlib.mobx", color: SkillColor.Green, value: 4 },
  AntDesign: { code: "reactlib.antdesign", color: SkillColor.Green, value: 4.5 },
  ChakraUI: { code: "reactlib.chakraui", color: SkillColor.Green, value: 4 },
};

export const StyleLib = {
  Tailwind: { code: "stylelib.tailwind", color: SkillColor.Green, value: 5 },
};

export const Styleguide = {
  BEM: { code: "styleguide.bem", color: SkillColor.Pink, value: 5 },
  FSD: { code: "styleguide.fsd", color: SkillColor.Pink, value: 2 },
  Airbnb: { code: "styleguide.airbnb", color: SkillColor.Pink, value: 5 },
  Google: { code: "styleguide.google", color: SkillColor.Pink, value: 5 },
  Agile: { code: "styleguide.agile", color: SkillColor.Pink, value: 5 },
  Kanban: { code: "styleguide.kanban", color: SkillColor.Pink, value: 5 },
};

export const Design = {
  Photoshop: { code: "design.photoshop", color: SkillColor.Pink, value: 5 },
  Illustrator: { code: "design.illustrator", color: SkillColor.Pink, value: 3.5 },
  Figma: { code: "design.figma", color: SkillColor.Pink, value: 4 },
};

export const Office = {
  Word: { code: "office.word", color: SkillColor.Pink, value: 5 },
  PowerPoint: { code: "office.powerpoint", color: SkillColor.Pink, value: 5 },
  Excel: { code: "office.excel", color: SkillColor.Pink, value: 3 },
};

export const Test = {
  Jest: { code: "test.jest", color: SkillColor.Blue, value: 3 },
  Vitest: { code: "test.vitest", color: SkillColor.Blue, value: 3 },
  Storybook: { code: "test.storybook", color: SkillColor.Blue, value: 3 },
};

export const IDE = {
  VSCode: { code: "ide.vscode", color: SkillColor.Blue, value: 4.5 },
  WebStorm: { code: "ide.webstorm", color: SkillColor.Blue, value: 4 },
  Brackets: { code: "ide.brackets", color: SkillColor.Blue, value: 5 },
};

export const Management = {
  Notion: { code: "management.notion", color: SkillColor.Yellow, value: 4 },
  Asana: { code: "management.asana", color: SkillColor.Yellow, value: 4 },
};

export const Other = {
  I18N: { code: "other.i18n", color: SkillColor.Green, value: 5 },
  MomentJs: { code: "other.moment", color: SkillColor.Green, value: 5 },
  DateFNS: { code: "other.datefns", color: SkillColor.Green, value: 5 },
};

export const Skill = {
  Tool,
  OS,
  Language,
  Build,
  Front,
  Back,
  Styling,
  Database,
  VueLib,
  ReactLib,
  StyleLib,
  Styleguide,
  Design,
  Office,
  Test,
  IDE,
  Management,
  Other,
};
