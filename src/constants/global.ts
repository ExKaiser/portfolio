import { ru, enGB } from 'date-fns/locale'
import { LangConfig } from "src/interfaces/global";

export const MaxProgressValue = 5;
export const DateFormat = 'dd MMMM yyyy'

export enum LangCode {
  Ru,
  En,
}

export const Lang: Record<LangCode, LangConfig> = {
  [LangCode.Ru]: {
    value: "ru",
    short: "Ru",
    labelCode: "languages.ru",
    dateFnsLocale: ru
  },
  [LangCode.En]: {
    value: "en",
    short: "En",
    labelCode: "languages.en",
    dateFnsLocale: enGB
  },
};

export const LanguageGrade = {
  Native: 'languages.native',
  Colloquial: 'languages.colloquial',
  Basic: 'languages.basic',
  Intermediate: 'languages.intermediate',
  Professional: 'languages.professional',
  A1: 'languages.A1',
  A2: 'languages.A2',
  B1: 'languages.B1',
  B2: 'languages.B2',
  C1: 'languages.C1',
  C2: 'languages.C2',
}