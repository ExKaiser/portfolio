import { Front, Language } from "src/constants/skills";
import { IProject } from "src/interfaces/projects";

export const ErgoKeyboard: IProject = {
  id: "ergo-keyboard",
  name: "ERGO Keyboard",
  cover: "1.png",
  year: 2018,
  links: {
    repo: "",
    web: "",
  },
  tags: {
    front: [Language.JavaScript, Language.CSS, Front.Vue2],
  },
  screens: ["1.png", "2.png", "3.png", "4.png"],
};
