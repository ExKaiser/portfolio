import { Front, VueLib, Language, Styling, Build } from "src/constants/skills";
import { IProject } from "src/interfaces/projects";

export const ReleaseGenerator: IProject = {
  id: "release-generator",
  name: "Release Generator",
  cover: "cover.png",
  company: "CRMM",
  year: 2019,
  links: {
    repo: "https://gitlab.com/crmm-legacy/release-generator",
    web: "",
  },
  tags: {
    front: [Language.TypeScript, Front.Vue3, Front.Electron, VueLib.Pinia, Styling.Scss, Build.Vite],
  },
  screens: ["1.png", "2.png", "3.png"],
};
