import { Front, Language, Styling, Build, Design, Test } from "src/constants/skills";
import { IProject } from 'src/interfaces/projects';

export const Medesk: IProject = {
  id: "medesk",
  name: "Medesk Client App",
  cover: "12.png",
  NDA: true,
  company: "ООО Medesk",
  year: 2020,
  links: {
    repo: "",
    web: "",
  },
  tags: {
    front: [
      Language.JavaScript,
      Front.React,
      Front.CoffeeScript,
      Front.Backbone,
      Front.Marionette,
      Styling.Less,
      Design.Figma,
      Test.Storybook,
      Build.Webpack,
      Build.Gulp
    ],
  },
  screens: [
    "1.png",
    "2.png",
    "3.png",
    "4.png",
    "5.png",
    "6.png",
    "7.png",
    "8.png",
    "9.png",
    "10.png",
    "11.png",
    "12.png",
  ],
};
