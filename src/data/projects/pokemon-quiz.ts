import { Front, VueLib, Language, Styling, StyleLib, Build, Back, Database, Other } from "src/constants/skills";

import { IProject } from "src/interfaces/projects";

export const PokemonQuiz: IProject = {
  id: "pokemon-quiz",
  name: "Pokemon Quiz",
  cover: "1.png",
  year: 2023,
  links: {
    repo: "https://gitlab.com/pokemon-quiz/pokemon-quiz-front",
    web: "https://pokemon-quiz.com",
  },
  tags: {
    front: [
      Language.TypeScript,
      Front.Vue3,
      Front.Axios,
      VueLib.Pinia,
      VueLib.Vuetify,
      Styling.Scss,
      StyleLib.Tailwind,
      Other.I18N,
      Build.Vite,
    ],
    back: [Back.NestJs, Back.PassportJs, Back.Sequelize, Database.Postgres],
  },
  screens: ["1.png", "2.png", "3.png", "4.png", "5.png", "6.png", "7.png", "8.png", "9.png", "10.png"],
};
