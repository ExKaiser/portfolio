import { Front, VueLib, Language, Styling, Build } from "src/constants/skills";
import { IProject } from "src/interfaces/projects";

export const PINPackageManager: IProject = {
  id: "pin-package-manager",
  name: "PIN Package Manager",
  cover: "1.png",
  year: 2021,
  links: {
    repo: "https://gitlab.com/pin-env/pin-packager",
    web: "",
  },
  tags: {
    front: [Language.JavaScript, Front.Vue2, VueLib.Quasar, Styling.Scss, Build.Webpack],
  },
  screens: ["1.png", "2.png", "3.png"],
};
