import { Front, Language, Styling, ReactLib, Other } from "src/constants/skills";
import { IProject } from "src/interfaces/projects";

export const Portfolio: IProject = {
  id: "portfolio",
  name: "Portfolio",
  cover: "1.png",
  year: 2023,
  links: {
    repo: "https://gitlab.com/ExKaiser/portfolio",
    web: "",
  },
  tags: {
    front: [
      Language.TypeScript,
      Front.React,
      ReactLib.Mobx,
      ReactLib.ChakraUI,
      Styling.Scss,
      Other.I18N,
    ],
  },
  screens: ["1.png", "2.png", "3.png"],
};
