import { Front, VueLib, Language, Styling, Build, Design } from "src/constants/skills";
import { IProject } from "src/interfaces/projects";

export const GtaProject: IProject = {
  id: "gta-project",
  name: "GTA Custom Server",
  cover: "1.png",
  NDA: true,
  company: "Immmersive Lab",
  year: 2022,
  links: {
    repo: "",
    web: "",
  },
  tags: {
    front: [Language.TypeScript, Front.Vue3, Front.Axios, VueLib.Vuex, Styling.Sass, Build.Vite, Design.Figma],
  },
  screens: [
    "1.png",
    "2.png",
    "3.png",
    "4.png",
    "5.png",
    "6.png",
    "7.png",
    "8.png",
    "9.png",
    "10.png",
    "11.png",
    "12.png",
    "13.png",
    "14.png",
    "15.png",
  ],
};
