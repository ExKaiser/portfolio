import { IProjects } from "src/interfaces/projects";

import { PokemonQuiz } from "./pokemon-quiz";
import { GtaProject } from "./gta-project";
import { WeatherApp } from "./weather-app";
import { ErgoKeyboard } from "./ergo-keyboard";
import { Medesk } from "./medesk";
import { PINPackageManager } from "./pin-package-manager";
import { ReleaseGenerator } from "./release-generator";
import { Portfolio } from "./portfolio";

export const projects: IProjects = [
  PokemonQuiz,
  GtaProject,
  Medesk,
  ReleaseGenerator,
  PINPackageManager,
  ErgoKeyboard,
  WeatherApp,
  Portfolio
];
