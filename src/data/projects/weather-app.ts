import { Build, Front, VueLib, Language, Other } from "src/constants/skills";
import { IProject } from "src/interfaces/projects";

export const WeatherApp: IProject = {
  id: "weather-app",
  name: "Weather App",
  cover: "1.png",
  year: 2021,
  links: {
    repo: "https://gitlab.com/ExKaiser/weather-widget",
    web: "",
  },
  tags: {
    front: [Language.TypeScript, Front.Vue2, Front.Axios, VueLib.Vuetify, Other.I18N, Build.Webpack],
  },
  screens: ["1.png", "2.png", "3.png"],
};
