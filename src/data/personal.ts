import { LanguageGrade } from "src/constants/global";
import { IPersonalInfo } from "src/interfaces/global";

export const personalInfo: IPersonalInfo = {
  name: "Петров Евгений Иванович",
  address: "personal.adressValue",
  birthdate: new Date(1994, 4, 20),
  // Финальное количество опыта рассчитывается как:
  // Текущая дата - Дата начала - Время пропуска
  expirience: {
    start: new Date(2016, 9, 20), // Дата начала профессионального опыта
    skip: [1, 0], // Общее время пропусков (в формате [годов, месяцев])
  },
  languages: [
    { textCode: "languages.ru", gradeCode: LanguageGrade.Native },
    { textCode: "languages.en", gradeCode: LanguageGrade.Intermediate },
    { textCode: "languages.de", gradeCode: LanguageGrade.Basic },
  ],
};
