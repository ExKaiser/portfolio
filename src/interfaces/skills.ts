import { SkillColor } from "src/constants/skills";

export type ISkillGroup = Record<string, ISkill>

export type ISkill = {
  code: string;
  color: SkillColor;
  value: number;
};

export type ISkillWithNs = ISkill & {
  ns: string;
};

export type IGradeGroups = Record<string, IGradeGroup>;

export type IGradeGroup = {
  textCode: string;
  className: string;
  data: ISkillGroup[];
};
