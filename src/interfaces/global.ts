import { Locale } from "date-fns";

export type IPersonalLanguage = {
  textCode: string;
  gradeCode: string
}

export type IPersonalInfo = {
  name: string;
  address: string;
  birthdate: Date;
  expirience: {
    start: Date;
    skip: [number, number];
  };
  languages: IPersonalLanguage[]
}

export interface LangConfig {
  value: string;
  short: string;
  labelCode: string;
  dateFnsLocale: Locale
}
