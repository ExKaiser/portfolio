import { ISkill } from './skills';

export type IProjects = IProject[];

export type IProject = {
  id: string;
  name: string;
  cover: string;
  NDA?: boolean;
  company?: string;
  year?: number;
  screens: string[];
  links: {
    repo?: string;
    web?: string;
  };
  tags: {
    front?: ISkill[];
    back?: ISkill[];
  };
};