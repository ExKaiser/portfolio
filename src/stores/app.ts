import { action, makeObservable, observable } from "mobx";

import i18n from "src/i18n/config";

import { Lang, LangCode } from "src/constants/global";
import { LangConfig } from "src/interfaces/global";

class AppStore {
  lang = Lang[LangCode.Ru];

  constructor() {
    makeObservable(this, {
      lang: observable,
      setLang: action.bound,
    });
  }

  setLang = (lang: LangConfig) => {
    this.lang = lang;
    i18n.changeLanguage(lang.value)
  };
}

const appStore = new AppStore();

export default appStore;
