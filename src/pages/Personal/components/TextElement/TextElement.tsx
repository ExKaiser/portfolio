import { Flex, Text } from '@chakra-ui/react'
import { DefaultTFuncReturn } from "i18next"

interface Props {
  text: string
  value: string | number | DefaultTFuncReturn
}

export const TextElement = (props: Props) => {
  return (
    <Flex justify='space-between'>
      <Text fontSize='sm' color='gray' align='start'>{props.text}: </Text>
      <Text fontSize='sm' fontWeight='bold' align='end'>{props.value}</Text>
    </Flex>
  )
}
