import { Flex, Heading, Text, Image } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'
import {
  intervalToDuration,
  differenceInYears,
  formatDuration,
  format,
  add
} from 'date-fns'

import { personalInfo } from 'src/data/personal'
import { DateFormat } from 'src/constants/global'
import appStore from 'src/stores/app'

import './Personal.scss'
import { TextElement } from './components/TextElement/TextElement'

const exp = personalInfo.expirience

export const Personal = () => {
  const { t } = useTranslation()

  const birthdate = format(personalInfo.birthdate, DateFormat, {
    locale: appStore.lang.dateFnsLocale
  })
  const age = differenceInYears(new Date(), personalInfo.birthdate)

  const computedStart = add(exp.start, {
    years: exp.skip[0],
    months: exp.skip[1]
  })

  const diff = intervalToDuration({ start: computedStart, end: new Date() })

  const totalExpirience = formatDuration(
    { years: diff.years, months: diff.months },
    {
      locale: appStore.lang.dateFnsLocale
    }
  )

  return (
    <div className='Personal'>
      <Image
        src='avatar.jpg'
        alt=''
        h='15rem'
        w='15rem'
        flexShrink={0}
        borderRadius='0.5rem'
      />

      <Flex direction='column' gap={1} w='70%'>
        <Flex direction='column' align='start' mb={6} gap={2}>
          <Heading size='md' textAlign='start'>{personalInfo.name}</Heading>
          <Heading size='xs' color='gray'>
            {t(personalInfo.address)}
          </Heading>
        </Flex>

        <TextElement text={t('personal.birthdate')} value={birthdate} />
        <TextElement text={t('personal.age')} value={age} />
        <TextElement text={t('personal.exp')} value={totalExpirience} />

        <Flex align='start' w='100%' justify='space-between'>
          <Text fontSize='sm' color='gray' align='left'>
            {t('personal.languages')}:
          </Text>

          <Flex direction='column' align='end' fontWeight='bold'>
            {personalInfo.languages.map((language, index) => (
              <Text fontSize='sm' key={index} align='end'>
                {t(language.textCode)} - {t(language.gradeCode)}
              </Text>
            ))}
          </Flex>
        </Flex>
      </Flex>
    </div>
  )
}
