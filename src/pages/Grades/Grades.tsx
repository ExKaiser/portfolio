import { Grid, GridItem } from "@chakra-ui/react"
import { GradeGroups } from 'src/constants/grade-groups'

import { GradeGroup } from './components/GradeGroup/GradeGroup'

import './Grades.scss'

export const Grades = () => {
  return (
    <div className='Grades'>
      <Grid gap={14}>
        {Object.entries(GradeGroups).map(([key, group], index) => (
          <GridItem key={index}>
            <GradeGroup group={group} />
          </GridItem>
        ))}
      </Grid>
    </div>
  )
}
