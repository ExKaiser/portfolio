import { Grid, GridItem, Heading } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next';

import { IGradeGroup } from 'src/interfaces/skills'

import { SkillsCard } from '../SkillsCard/SkillsCard'

import './GradeGroup.scss'

interface Props {
  group: IGradeGroup
}

export const GradeGroup = (props: Props) => {
  const { t } = useTranslation();

  return (
    <div className='GradeGroup'>
      <Heading size='sm' mb='6'>
        {t(props.group.textCode)}
      </Heading>

      <Grid templateColumns='repeat(2, 1fr)' gap={5}>
        {props.group.data.map((skills, index) => (
          <GridItem key={index} h='100%'>
            <SkillsCard data={skills} />
          </GridItem>
        ))}
        {/* </div> */}
      </Grid>
    </div>
  )
}
