import { Card, CardHeader, CardBody, Heading, Stack } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

import { ISkillGroup } from 'src/interfaces/skills'

import { Skill } from '../Skill/Skill'

import './SkillsCard.scss'

interface Props {
  data: ISkillGroup
}

export const SkillsCard = (props: Props) => {
  const { t } = useTranslation()

  const ns = Object.values(props.data)[0].code.split('.')[0]

  return (
    <div className='SkillsCard'>
      <Card h='100%'>
        <CardHeader alignSelf='start' pb={0}>
          <Heading size='xs' textAlign='start' color='rgb(80, 80, 80)'>
            {t(`skills.${ns}.name`)}
          </Heading>
        </CardHeader>

        <CardBody>
          <Stack>
            {Object.entries(props.data).map(([key, skill], index) => (
              <Skill skill={skill} key={index} />
            ))}
          </Stack>
        </CardBody>
      </Card>
    </div>
  )
}
