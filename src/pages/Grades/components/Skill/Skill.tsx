import { Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next';

import { Progress } from 'src/components/Progress/Progress'
import { ISkill } from 'src/interfaces/skills'

import './Skill.scss'

interface Props {
  skill: ISkill
}

export const Skill = (props: Props) => {
  const { t } = useTranslation();

  return (
    <div className='Skill'>
      <div className="Skill-label">
        <Text fontSize="xs" align='start'>{t(`skills.${props.skill.code}`)}</Text>
      </div>

      <div className='Skill-progress'>
        <Progress value={props.skill.value} showValue />
      </div>
    </div>
  )
}
