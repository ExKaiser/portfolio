import { useEffect, useRef, useState } from 'react'
import {
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Stack,
  useDisclosure
} from '@chakra-ui/react'
import { SearchIcon, CloseIcon } from '@chakra-ui/icons'
import { useTranslation } from 'react-i18next'

import { projects } from 'src/data/projects'
import { IProject } from 'src/interfaces/projects'
import { ProjectModal } from 'src/components/ProjectModal/ProjectModal'

import { ProjectPreview } from './components/ProjectPreview/ProjectPreview'

export const ProjectList = () => {
  const { t } = useTranslation()

  const placeholder = t('projects.placeholder')

  const [searchValue, setSearchValue] = useState('')
  const [filtered, setFiltered] = useState(projects)
  const [projectForModal, setProjectForModal] = useState<IProject | null>(null)
  const { isOpen, onOpen, onClose } = useDisclosure()
  const inputRef = useRef<HTMLInputElement | null>(null)

  const handleChange = (event: any) => {
    setSearchValue(event.target?.value.toLowerCase())
  }

  const showProjectModal = (project: IProject) => {
    setProjectForModal(project)
    onOpen()
  }

  const hideProjectModal = () => {
    setProjectForModal(null)
    onClose()
  }

  const clearInput = () => {
    setSearchValue('')

    if (inputRef.current) {
      inputRef.current.value = ''
    }
  }

  useEffect(() => {
    if (!searchValue) {
      return setFiltered(projects)
    }

    const result = projects.filter(project => {
      let matchFrontTags = false
      let matchBackTags = false

      if (project.tags.front?.length) {
        matchFrontTags = project.tags.front?.some(
          skill => skill.code.indexOf(searchValue) !== -1
        )
      }

      if (project.tags.back?.length) {
        matchBackTags = project.tags.back.some(
          skill => skill.code.indexOf(searchValue) !== -1
        )
      }

      return matchFrontTags || matchBackTags
    })

    setFiltered(result)
  }, [searchValue])

  return (
    <div className='ProjectList'>
      <InputGroup>
        <InputLeftElement
          pointerEvents='none'
          children={<SearchIcon color='gray.400' />}
        />
        <Input
          placeholder={placeholder}
          onChange={handleChange}
          ref={inputRef}
        />

        {!!searchValue.length && (
          <InputRightElement
            cursor='pointer'
            children={<CloseIcon color='gray.400' />}
            onClick={() => clearInput()}
          />
        )}
      </InputGroup>

      <Stack gap={2} mt={8}>
        {filtered.map((project, index) => (
          <div key={index} onClick={() => showProjectModal(project)}>
            <ProjectPreview project={project} />
          </div>
        ))}
      </Stack>

      {isOpen && projectForModal && (
        <ProjectModal project={projectForModal} onClose={hideProjectModal} />
      )}
    </div>
  )
}
