import { Card, CardBody, Flex, Heading, Image, Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

import { IProject } from 'src/interfaces/projects'
import { TagGroup } from 'src/components/TagGroup/TagGroup'

import './ProjectPreview.scss'

interface Props {
  project: IProject
}

export const ProjectPreview = ({ project }: Props) => {
  const { t } = useTranslation()
  const coverSrc = `/projects/${project.id}/${project.cover}`

  const tags = [...(project.tags.front || []), ...(project.tags.back || [])]

  return (
    <Card
      className='ProjectPreview'
      h='100%'
      size='sm'
      variant='unstyled'
      borderRadius='0.6rem'
      cursor='pointer'
    >
      <CardBody>
        <Flex gap={16}>
          <Image
            borderRadius='6'
            h='10rem'
            w='12rem'
            fit='cover'
            src={coverSrc}
            loading='lazy'
            flexShrink='0'
            alt=''
          />

          <Flex direction='column' gap={4} align='start' w='100%'>
            <Heading size='md'>{project.name}</Heading>

            <TagGroup data={tags} />

            <Text fontSize='sm' align='left' w='80%'>
              {t(`projects.${project.id}.brief`)}
            </Text>
          </Flex>
        </Flex>
      </CardBody>
    </Card>
  )
}
