import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalCloseButton
} from '@chakra-ui/react'
import { IProject } from 'src/interfaces/projects'

import { Carousel } from './components/Carousel/Carousel'
import { Content } from './components/Content/Content'

interface Props {
  project: IProject
  onClose: Function
}

export const ProjectModal = ({ project, onClose }: Props) => {
  const handleClose = () => {
    onClose()
  }

  return (
    <Modal isOpen onClose={handleClose} isCentered size='6xl'>
      <ModalOverlay overflow='hidden' />

      <ModalContent overflow='hidden' my={0}>
        <ModalCloseButton />

        <ModalBody py='3rem' px='5rem'>
          <Carousel project={project} />
          <Content project={project} />
        </ModalBody>
      </ModalContent>
    </Modal>
  )
}
