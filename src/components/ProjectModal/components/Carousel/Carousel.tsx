import { Image } from '@chakra-ui/react'
import { Navigation, Pagination, Autoplay } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import 'swiper/css/autoplay'

import { IProject } from 'src/interfaces/projects'

interface Props {
  project: IProject
}

export const Carousel = ({ project }: Props) => {
  const getScreenSrc = (screen: string) =>
    `/projects/${project.id}/${screen}`

  return (
    <Swiper
      modules={[Navigation, Pagination, Autoplay]}
      slidesPerView={1}
      navigation
      loop
      autoplay={{
        delay: 8000
      }}
      pagination={{ clickable: true }}
    >
      {project.screens.map((screen, index) => (
        <SwiperSlide key={index}>
          <Image
            src={getScreenSrc(screen)}
            h='30rem'
            w='100%'
            fit='contain'
            alt=''
          />
        </SwiperSlide>
      ))}
    </Swiper>
  )
}
