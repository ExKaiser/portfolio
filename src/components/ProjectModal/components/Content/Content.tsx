import { Flex, Heading, Text } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

import { NDA } from 'src/components/NDA/NDA'
import { TagGroup } from 'src/components/TagGroup/TagGroup'
import { IProject } from 'src/interfaces/projects'

import { SubheaderText } from '../SubheaderText/SubheaderText'

interface Props {
  project: IProject
}

export const Content = ({ project }: Props) => {
  const { t } = useTranslation()
  const description: string[] = t(`projects.${project.id}.description`, {
    returnObjects: true
  })

  const PortfolioWebLink = () => (
    <SubheaderText subheader={t('modal.web')} text={t('other.alreadyHere')} />
  )

  return (
    <Flex direction='column' mt='2rem' gap='1rem'>
      <Flex gap={4} align='end'>
        <Heading size='md'>{project.name}</Heading>
        {project.NDA && <NDA />}
      </Flex>

      <Flex gap={24}>
        <Flex direction='column' align='start' flex={1} gap={2}>
          {project.company && (
            <SubheaderText
              subheader={t('modal.company')}
              text={project.company}
            />
          )}

          {project.year && (
            <SubheaderText subheader={t('modal.date')} text={project.year} />
          )}

          {project.links.repo && !project.NDA && (
            <SubheaderText
              type='link'
              subheader={t('modal.repo')}
              text={project.links.repo}
            />
          )}

          {project.links.web && (
            <SubheaderText
              type='link'
              subheader={t('modal.web')}
              text={project.links.web}
            />
          )}

          {project.id === 'portfolio' && (
            <PortfolioWebLink />
          )}

          {(!!project.tags.front || !!project.tags.back) && (
            <Flex direction='column' mt={4} gap={2}>
              {project.tags.front && <TagGroup data={project.tags.front} />}
              {project.tags.back && <TagGroup data={project.tags.back} />}
            </Flex>
          )}
        </Flex>

        {!!description.length && (
          <Flex direction='column' flex={1} gap={2}>
            {description.map((text: string, index: number) => (
              <Text fontSize='13px' key={index}>
                {text}
              </Text>
            ))}
          </Flex>
        )}
      </Flex>
    </Flex>
  )
}
