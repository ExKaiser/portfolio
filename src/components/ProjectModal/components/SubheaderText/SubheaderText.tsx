import { ExternalLinkIcon } from '@chakra-ui/icons'
import { Flex, Link, Text } from '@chakra-ui/react'
import { DefaultTFuncReturn } from "i18next"

interface Props {
  subheader: string
  text: string | number | DefaultTFuncReturn
  type?: 'link'
}

export const SubheaderText = (props: Props) => {
  return (
    <Flex direction='column'>
      <Text fontSize='xs' fontWeight='bold' color='gray'>
        {props.subheader}
      </Text>

      {props.type === 'link' && (
        <Link
          href={props.text as string}
          color='blue.700'
          fontSize='sm'
          isExternal
        >
          {props.text} <ExternalLinkIcon ml='0.2rem' mb='0.1rem' />
        </Link>
      )}

      {!props.type && <Text fontSize='sm'>{props.text}</Text>}
    </Flex>
  )
}
