import { Tag, Tooltip } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

export const NDA = () => {
  const { t } = useTranslation()

  return (
    <Tooltip
      label={t('other.NDATooltip')}
      placement='top'
      fontSize='xs'
      py={2}
      px={3}
      borderRadius={5}
      w='18rem'
    >
      <Tag size='sm' variant='solid' colorScheme='red' userSelect='none'>
        {t('other.NDA')}
      </Tag>
    </Tooltip>
  )
}
