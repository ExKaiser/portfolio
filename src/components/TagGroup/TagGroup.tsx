import { Flex, Tag } from '@chakra-ui/react'
import { useTranslation } from 'react-i18next'

import { ISkill } from 'src/interfaces/skills'
import { SkillColor } from 'src/constants/skills'

interface Props {
  data: ISkill[]
}

const TagColorMap = {
  [SkillColor.Green]: 'teal',
  [SkillColor.Yellow]: 'yellow',
  [SkillColor.Blue]: 'blue',
  [SkillColor.Pink]: 'pink'
}

export const TagGroup = (props: Props) => {
  const { t } = useTranslation()

  return (
    <Flex gap={1} wrap='wrap'>
      {props.data.map((skill, index) => (
        <Tag
          size='sm'
          key={index}
          variant='solid'
          colorScheme={TagColorMap[skill.color]}
          userSelect='none'
        >
          {t(`skills.${skill.code}`)}
        </Tag>
      ))}
    </Flex>
  )
}
