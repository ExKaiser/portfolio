import { MaxProgressValue } from 'src/constants/global'

import './Progress.scss'

const getColor = (pct = 100) => {
  const colorStart = `hsla(${pct}, 95%, 40%, 1)`
  const colorEnd = `hsla(${pct}, 100%, 50%, 1)`

  return `linear-gradient(to right, ${colorStart} 0%, ${colorEnd} 100%)`
}

interface Props {
  value: number
  showValue?: boolean
}

// {Array(MaxProgressValue)
//   .fill(true)
//   .map((elem, index) => (
//     <div className='Progress-star' key={index}>
//       111
//     </div>
//   ))}

export const Progress = (props: Props) => {
  const percentValue = (props.value * 100) / MaxProgressValue

  const style = {
    width: `${percentValue}%`,
    background: getColor(percentValue)
  }

  return (
    <div className='Progress'>
      <div className='Progress-line'>
        <div className='Progress-line-fill' style={style} />
      </div>

      {props.showValue && <span className='Progress-value'>{props.value}</span>}
    </div>
  )
}
