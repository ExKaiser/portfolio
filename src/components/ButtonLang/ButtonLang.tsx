import {
  Menu,
  MenuButton,
  MenuList,
  Button,
  MenuOptionGroup,
  MenuItemOption
} from '@chakra-ui/react'
import { observer } from 'mobx-react'
import { useTranslation } from 'react-i18next'

import { Lang } from 'src/constants/global'
import { LangConfig } from 'src/interfaces/global'
import appStore from 'src/stores/app'

export const ButtonLang = observer(() => {
  const { t } = useTranslation()

  const onMenuItemClick = (lang: LangConfig) => {
    appStore.setLang(lang)
  }

  return (
    <div className='ButtonLang'>
      <Menu>
        <MenuButton as={Button} aria-label='Options' variant='outline'>
          {appStore.lang.short}
        </MenuButton>

        <MenuList>
          <MenuOptionGroup defaultValue={Lang[0].value} type='radio'>
            {Object.entries(Lang).map(([key, lang]) => (
              <MenuItemOption
                onClick={() => onMenuItemClick(lang)}
                key={lang.value}
                value={lang.value}
              >
                {t(lang.labelCode)}
              </MenuItemOption>
            ))}
          </MenuOptionGroup>
        </MenuList>
      </Menu>
    </div>
  )
})
