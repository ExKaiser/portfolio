import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import ru from './locales/ru.json'
import en from './locales/en.json'

i18n.use(initReactI18next).init({
  fallbackLng: 'ru',
  lng: 'ru',
  resources: {
    ru: {
      translations: ru
    },
    en: {
      translations: en
    }
  },
  ns: ['translations'],
  defaultNS: 'translations'
});

i18n.languages = ['ru', 'en'];

export default i18n;